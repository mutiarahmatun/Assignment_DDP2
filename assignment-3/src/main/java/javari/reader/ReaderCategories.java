package src.main.java.javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class ReaderCategories extends CsvReader{

    protected String[] type;
    protected String[] category;
    protected String[] sections;

    public static final List<String> cirOfFire = Arrays.asList("lions", "whales", "eagles");
    public static final List<String> danMal = Arrays.asList("parrots", "snakes", "cats", "hamsters");
    public static final List<String> countMas = Arrays.asList("hamsters", "whales", "parrots");
    public static final List<String> passCode = Arrays.asList("hamsters", "cats", "snakes");

    public ReaderCategories (Path file) throws IOException{
        super(file);
        for (String lines:this.getLines()) {
            String[] newLine = lines.split(COMMA);
            type = type.add(newLine[0]);
            category = category.add(newLine[1]);
            sections = sections.add(newLine[2]);
        }
    }

    public long countValidRecords() {
        long mammal = 0;
        long aves = 0;
        long reptile = 0;

        for(String line: this.lines) {
            String[] lineSplit = line.split(COMMA);
            if(lineSplit[1].equalsIgnoreCase("mammals") && mammals.contains(lineSplit[0].toLowerCase())) {
                mammal = 1;
            } else if(lineSplit[1].equalsIgnoreCase("aves") && avess.contains(lineSplit[0].toLowerCase())) {
                aves = 1;
            } else if(lineSplit[1].equalsIgnoreCase("reptiles") && reptiles.contains(lineSplit[0].toLowerCase())) {
                reptile = 1;
            }
        }
        return mammal + aves + reptile;

    }

    public long countInvalidRecords() {}
}