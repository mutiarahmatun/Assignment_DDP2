package src.main.java.javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ReaderRecords extends CsvReader{

    protected String[] id;
    protected String[] type;
    protected String[] animalName;
    protected String[] gender;
    protected String[] bodyLengthCm;
    protected String[] bodyWeightKg;
    protected String[] specialStatus;
    protected String[] condition;

    public ReaderRecords (Path file) throws IOException{
        super(file);
        for (String lines:this.getLines()) {
            String[] newLine = lines.split(COMMA);
            id = id.add(newLine[0]);
            type = type.add(newLine[1]);
            animalName = animalName.add(newLine[2]);
            gender = gender.add(newLine[3]);
            bodyLengthCm = bodyLengthCm.add(newLine[4]);
            bodyWeightKg = bodyWeightKg.add(newLine[5]);
            specialStatus = specialStatus.add(newLine[6]);
            condition = condition.add(newLine[7]);
        }
    }

    public long countValidRecords() {

    }

    public long countInvalidRecords() {}
}