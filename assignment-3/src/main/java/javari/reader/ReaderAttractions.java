package src.main.java.javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class ReaderAttractions extends CsvReader{

    private String[][] type;
    private String[][] attraction;
    private int valid = 0;

    public static final List<String> cirOfFire = Arrays.asList("lions", "whales", "eagles");
    public static final List<String> danMal = Arrays.asList("parrots", "snakes", "cats", "hamsters");
    public static final List<String> countMas = Arrays.asList("hamsters", "whales", "parrots");
    public static final List<String> passCode = Arrays.asList("hamsters", "cats", "snakes");

    public ReaderAttractions (Path file) throws IOException{
        super(file);
    }

    public long countValidRecords() {

    }

    public long countInvalidRecords() {

    }
}