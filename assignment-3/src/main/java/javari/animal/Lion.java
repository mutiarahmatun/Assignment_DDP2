package src.main.java.javari.animal;

public class Lion extends Animal {

    private String specialCondition;

    public Lion (Integer id, String type, String name, Gender gender, double length,
                 double weight, String specialCondition, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialCondition = specialCondition;
    }

    protected boolean specificCondition() {
        if (specialCondition.equals("") && getGender().equals("male")){
            return true;
        }else if (specialCondition.equals("pregnant")){
            return false;
        }
    }
}