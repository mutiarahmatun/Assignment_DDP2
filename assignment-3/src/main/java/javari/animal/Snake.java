package src.main.java.javari.animal;

public class Snake extends Animal {

    private String specialCondition;

    public Snake (Integer id, String type, String name, Gender gender, double length,
                 double weight, String specialCondition, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialCondition = specialCondition;
    }

    protected boolean specificCondition() {
        if (specialCondition.equals("tame")){
            return true;
        }else if (specialCondition.equals("")){
            return false;
        }
    }
}
