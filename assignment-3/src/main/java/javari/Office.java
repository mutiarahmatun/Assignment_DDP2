package src.main.java.javari;

import java.util.Scanner;
import src.main.java.javari.park.*;
import src.main.java.javari.reader.*;
import src.main.java.javari.writer.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class Office{

    public static Scanner input = new Scanner(System.in);

    private CsvReader readerRecords;
    private CsvReader readerAttractions;
    private CsvReader readerCategories;
    private Path File;

    public static void main(String args[]){

        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println("\n... Opening default section database from data. ... File not found or incorrect file!");
        System.out.print("\nPlease provide the source data path: ");
        String data = input.next();

        System.out.println("\n... Loading... Success... System is populating data...");

        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println("Please answer the questions by typing the number." +
                            " Type # if you want to return to the previous menu");
    }
}