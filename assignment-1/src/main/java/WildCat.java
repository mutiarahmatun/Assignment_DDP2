public class WildCat {
    // This class is to represents the cat that will be transported to Javari Park

    public String name;
    public double weight; // In kilograms
    public double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        // A variable
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        // Body Mass Index (BMI)
        return (weight / ((length * length) / 10000.0));
    }
}
