import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    public static double trainWeight = 0;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = Integer.parseInt(input.nextLine());
        int hitungKucing = 0;
        TrainCar car = null;
        while (true) {
            for (int jumlah = 0; jumlah < n; jumlah++) {
                String catDescription = input.nextLine();
                String[] descriptionArray = catDescription.split(",");
                String nameCat = descriptionArray[0];
                double weightCat = Double.parseDouble(descriptionArray[1]);
                double lengthCat = Double.parseDouble(descriptionArray[2]);

                WildCat cat = new WildCat(nameCat, weightCat, lengthCat);
                if (car == null) {
                    car = new TrainCar(cat);
                } else {
                    car = new TrainCar(cat, car);
                }
                // hitung total semuua berat kucing
                trainWeight = car.computeTotalWeight();
                hitungKucing = hitungKucing + 1;

                if (trainWeight > THRESHOLD) {
                    break;
                }
            }

            System.out.println("The train departs to Javari Park");
            System.out.print("[LOCO]<--");
            car.printCar();
            double average = car.computeTotalMassIndex() / hitungKucing;
            System.out.println("Average mass index of all cats: " + String.format("%.2f", average));

            if (average < 18.5) {
                System.out.println("In average, the cats in the train are *underweight*");
            } else if (average >= 18.5 && average < 25) {
                System.out.println("In average, the cats in the train are *normal*");
            } else if (average >= 25 && average < 30) {
                System.out.println("In average, the cats in the train are *overweight*");
            } else {

                System.out.println("In average, the cats i the train are *obese*");
            }
            n = n - hitungKucing;
            car = null;
            if (n == 0) {
                System.exit(0);
            }
            hitungKucing = 0;
        }
    }
}