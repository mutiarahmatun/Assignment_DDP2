package board;

import card.Card;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collections;

public class Board {

    /**
     * An instance variables
     */
    private String path;
    private String[] img;
    private Card card1;
    private Card card2;
    private Timer timer;
    private JPanel panel;
    private int counter;
    private JLabel label;

    /**
     * Constructor
     * Initializes a Board with given label
     */
    public Board(JLabel label){
        this.label = label;
        this.path = "D:\\Tugas Pemrograman\\assignment-4\\img\\";
        this.img = new String[]{"1", "2", "3", "4", "5", "6",
                "7", "8", "9", "10", "11", "12", "13", "14",
                "15", "16", "17", "18"};
        this.panel = new JPanel();
        panel.setLayout(new GridLayout(6,6));

        addCards(path, img, panel);
        addCards(path, img, panel);

        this.timer = new Timer(750, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkCard();
            }
        });

        timer.setRepeats(false);
    }

    /**
     * Accessor for Panel field.
     * @return panel of this board instance
     */
    public JPanel getPanel() {
        return panel;
    }

    /**
     * inisiasi button(card) dengan meletakkannya secara acak
     * @param path path image yang diletakkan pada card
     * @param img nama file image
     * @param panel tempat untuk card yang akan diletakkan
     */
    private void addCards(String path, String[] img, JPanel panel) {
        Card card;
        Collections.shuffle(Arrays.asList(img));
        for(int i = 0; i < 18; i++) {
            card = new Card(new ImageIcon(path + img[i] + ".jpg"));
            card.addActionListener(new cardListener(card));
            card.setSize(100,100);
            card.setMaximumSize(new Dimension(100,150));
            card.setMinimumSize(new Dimension(100,100));
            panel.add(card);
        }
    }

    /**
     * Method yang digunakan untuk mengecek card setelah card kedua telah dibuka
     * setelah dicek card akan ditutup kembali jika tidak sama
     * jika sama card akan di disable dari game
     */
    private void checkCard() {
        card1.changeCover();
        card2.changeCover();
        if(card2 != null) {
            if (card1.getBackGround().getDescription().equals(card2.getBackGround().getDescription())) {
                card1.setVisible(false);
                card2.setVisible(false);
            }
            card1 = null;
            card2 = null;
        }
    }

    /**
     * Method untuk membuka card ketika diklik
     * @param c
     */
    private void openCard(Card c) {
        if(card1 == null) {
            card1 = c;
            card1.changeBackGround();
        } else if(card2 == null && c != card1) {
            card2 = c;
            card2.changeBackGround();
            counter++;
            label.setText("Number of Tries: " + counter);
            timer.start();
        }
    }


    class cardListener implements ActionListener {

        private Card c;
        public cardListener(Card card) {
            this.c = card;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            openCard(c);
        }
    }
}