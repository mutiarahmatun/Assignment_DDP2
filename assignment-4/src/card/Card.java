package card;

import javax.swing.*;

/**
 * Class representing a card
 */
public class Card extends JButton
{
    /**
     * An instance variables for representing a cover and a back image
     */
    private ImageIcon cover;
    private ImageIcon backGround;

    /**
     * Constructor
     * Initializes a Card object with given backGround
     */
    public Card(ImageIcon backGround) {
        this.backGround = backGround;
        this.cover = new ImageIcon("D:\\Tugas Pemrograman\\assignment-4\\img\\cover.jpg");
        setIcon(this.cover);
    }

    /**
     * Accessor for backGround field.
     * @return backGround of this game instance
     */
    public ImageIcon getBackGround() {
        return backGround;
    }

    /**
     * Method untuk mengubah gambar card dengan gambar covernya
     */
    public void changeCover() {
        setIcon(cover);
    }

    /**
     * Method untuk mengubah gambar card dengan gambar backgroundnya
     */
    public void changeBackGround() {
        setIcon(backGround);
    }
}