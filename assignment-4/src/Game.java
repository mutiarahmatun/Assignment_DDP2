import board.Board;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class representing game managing system
 */
public class Game {

    /**
     * Constructor
     */
    public Game() {
        JFrame frame = new JFrame();
        frame.setTitle("Match Pair Mutia");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        JPanel buttonPanel = new JPanel();

        JButton reset = initButton("Play again?", frame);
        JButton exit = initButton("Exit", frame);

        buttonPanel.add(reset);
        buttonPanel.add(exit);

        frame.setSize(600, 1000);
        frame.setResizable(false);
        JLabel label = new JLabel("Number of Tries: 0", JLabel.CENTER);

        // INIT BOARD GAME
        Board board = new Board(label);

        frame.add(board.getPanel(), BorderLayout.NORTH);
        frame.add(buttonPanel, BorderLayout.CENTER);
        frame.add(label, BorderLayout.SOUTH);

        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Game();
            }
        });
    }

    /**
     * Method untuk menginiisasi kegunaan dari buttonPanel
     * @param text perintah yang ingin dijalankan
     * @param frame frame yang akan digunakan
     * @return Event nama object event yang dicari, return null apabila tidak ditemukan
     */
    private JButton initButton(String text, JFrame frame) {
        JButton res = new JButton(text);
        if(text.equals("Play again?")) {
            res.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            frame.dispose();
                            new Game();
                        }
                    });
                }
            });
        } else if(text.equals("Exit")) {
            res.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
            });
        }
        return res;
    }
}