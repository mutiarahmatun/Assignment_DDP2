import animals.*;
import cages.*;

import java.util.*;

public class Office{

    public static Scanner input = new Scanner(System.in);
    public static ArrayList<Animals> cat = new ArrayList<Animals>(),
            lion = new ArrayList<Animals>(),
            eagle = new ArrayList<Animals>(),
            parrot = new ArrayList<Animals>(),
            hamster = new ArrayList<Animals>();
    public static Cages catCages, lionCages, eagleCages, parrotCages, hamsterCages;
    public static Map<String, ArrayList<Animals>> hewanKu = createMap();

	public static void main(String[] args) {

        System.out.println("Welcome to Javari Park!");

        memprosesInputan();

        memprosesKandang();

        String masukan, namaHewan;

        String[] animalsName = {"cat", "eagle", "hamster", "parrot", "lion"};

        while (true) {
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            masukan = input.nextLine();

            if (masukan.equals("99")) {
                System.out.println("Bye!!");
                break;
            } else if (Integer.parseInt(masukan) > 5) {
                System.out.println("You do nothing...\n");
                continue;
            }

            namaHewan = animalsName[Integer.parseInt(masukan) - 1];

            System.out.print("Mention the name of " + namaHewan + " you want to visit: ");
            masukan = input.nextLine();

            boolean check = false;

            Animals hewan = null;

            for (Animals hewanRn : hewanKu.get(namaHewan)) {
                if (hewanRn.getName().equals(masukan)) {
                    check = true;
                    hewan = hewanRn;
                    break;
                }
            }

            if (check) {
                System.out.println("You are visiting " + masukan + " (" + namaHewan + ") now, what would you like to do?");
                if (namaHewan.equals("cat"))
                    Cats.class.cast(hewan).activity();
                else if (namaHewan.equals("eagle"))
                    Eagle.class.cast(hewan).activity();
                else if (namaHewan.equals("hamster"))
                    Hamsters.class.cast(hewan).activity();
                if (namaHewan.equals("parrot"))
                    Parrots.class.cast(hewan).activity();
                if (namaHewan.equals("lion"))
                    Lion.class.cast(hewan).activity();
            }else
                System.out.print("There is no " + namaHewan + " with that name!");

            System.out.println("Back to the office!\n");
        }
    }

    public static void memprosesInputan(){

	    String[] masukan, info;
	    String[] listHewan = {"cat", "lion", "eagle", "parrot", "hamster"};
	    int n;

        System.out.println("Input the number of animals");

        for (String animal : listHewan){
            System.out.print(animal + ": ");
            n = Integer.parseInt(input.nextLine());
            if (n > 0){
                System.out.println("Provide the information of " + animal + "(s):");
                masukan = input.nextLine().split(",");

                for (int a = 0; a < n; a++) {
                    info = masukan[a].split("\\|");
                    hewanKu.get(animal).add(createAnimal(animal, info[0], Integer.parseInt(info[1])));
                }
            }
        }

        System.out.println("Animals has been successfully recorded!\n");
	}

	public static void memprosesKandang(){

	    if (!cat.isEmpty()) {
	        catCages = new Cages(cat, "indoor");
        }
        if (!lion.isEmpty()) {
            lionCages = new Cages(lion, "outdoor");
        }
        if (!eagle.isEmpty()) {
            eagleCages = new Cages(eagle, "outdoor");
        }
        if (!parrot.isEmpty()) {
            parrotCages = new Cages(parrot, "indoor");
        }
        if (!hamster.isEmpty()) {
            hamsterCages = new Cages(hamster, "indoor");
        }

        Cages.arrangementCages();
    }

    public static Animals createAnimal(String namaHewan, String name, int length) {
	    if (namaHewan.equals("cat"))
	        return new Cats(name, length);
        if (namaHewan.equals("lion"))
            return new Lion(name, length);
        if (namaHewan.equals("eagle"))
            return new Eagle(name, length);
        if (namaHewan.equals("parrot"))
            return new Parrots(name, length);
        if (namaHewan.equals("hamster"))
            return new Hamsters(name, length);
        return null;
    }

	public static Map<String, ArrayList<Animals>> createMap() {
	    Map<String, ArrayList<Animals>> map = new HashMap<String, ArrayList<Animals>>();
	    map.put("cat", cat);
	    map.put("lion", lion);
	    map.put("eagle", eagle);
	    map.put("parrot", parrot);
	    map.put("hamster", hamster);

	    return map;
    }
}