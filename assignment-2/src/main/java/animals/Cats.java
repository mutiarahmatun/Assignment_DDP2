package animals;

import java.util.Scanner;
import java.util.Random;

public class Cats extends Animals{

	public Cats(String name, int length){
		super(name, length, "cat");
	}

	public void activity(){

	    Scanner input = new Scanner(System.in);
	    Random s = new Random();
        String[] suara = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};

        System.out.println("1: Brush the fur 2: Cuddle");
        String masukan = input.nextLine();

        if (masukan.equals("1")){
            System.out.println("Time to clean " + this.name + "'s fur");
            System.out.println(this.name + " makes a voice: Nyaaan...");
        }else if(input.equals("2")){
            System.out.println(this.name + " make a voice: " + suara[s.nextInt(4)]);
        }else{
            System.out.println("You do nothing...");
        }
    }
}