package animals;

import java.util.Scanner;

public class Hamsters extends Animals{

	public Hamsters(String name, int length){
		super(name, length, "hamster");
	}

    public void activity() {

        Scanner input = new Scanner(System.in);

        System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
        String masukan = input.nextLine();

        if (masukan.equals("1")) {
            System.out.println(name + " makes a voice: ngkkrit.. ngkkrrriiit");
        } else if (masukan.equals("2")) {
            System.out.println(name + " makes a voice: trrr…. trrr...");
        } else {
            System.out.println("You do nothing...");
        }
    }
}