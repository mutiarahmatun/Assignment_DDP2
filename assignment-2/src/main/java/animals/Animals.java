package animals;

public class Animals{
    protected String name, namaHewan;
	protected int length;

	public Animals(String name, int length, String namaHewan){
	    this.name = name;
	    this.length = length;
	    this.namaHewan = namaHewan;
    }

	public String getName(){return name;};
	public int getLength(){return length;};
	public String getNamaHewan() {return namaHewan;}

}