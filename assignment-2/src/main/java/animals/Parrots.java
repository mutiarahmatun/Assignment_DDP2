package animals;

import java.util.Scanner;

public class Parrots extends Animals {

    public Parrots(String name, int length) {
        super(name, length, "parrot");
    }

    public void activity() {

        Scanner input = new Scanner(System.in);

        System.out.println("1: Order to fly 2: Do conversation");
        String masukan = input.nextLine();

        if (masukan.equals("1")) {
            System.out.println("Parrot " + name + " flies!");
            System.out.println(name + " makes a voice: FLYYYY…..");
        } else if (masukan.equals("2")) {
            System.out.println("You say: ");
            String kata = input.nextLine();
            System.out.print(name + " says: " + kata.toUpperCase());
        } else {
            System.out.println(name + " says: HM?");
        }
    }
}