package cages;

import java.util.*;
import animals.*;

public class Cage {

    private Animals animal;
    private String type = "C";

    public Cage(Animals animal, String tempat) {
        this.animal = animal;

        if (tempat.equals("indoor")) {
            this.type = (animal.getLength() < 61) ? "B" : this.type;
            this.type = (animal.getLength() < 45) ? "A" : this.type;
        }else {
            this.type = (animal.getLength() < 91) ? "B" : this.type;
            this.type = (animal.getLength() < 75) ? "A" : this.type;
        }
    }
    public boolean isValid(){
        if(animal == null){ return false; }
        else { return true;}
    }

    public String toString() {
        return " " + animal.getName() + " (" + animal.getLength() +
                " - " + type + "),";
    }
}