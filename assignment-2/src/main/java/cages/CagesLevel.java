package cages;

import java.util.*;
import animals.*;

class CagesLevel{

    private ArrayList<Cage> kandang;

    public CagesLevel(){
        this.kandang = new ArrayList<Cage>();
    }

    public void add(Cage kandangs){
        kandang.add(kandangs);
    }

    public boolean isEmpty(){
        return kandang.isEmpty();
    }

    public ArrayList<Cage> getKandang() {
        return kandang;
    }

    public void balik(){
        ArrayList<Cage> levelBaru = new ArrayList<Cage>();

        for (int i = kandang.size()-1; i >= 0; i--) {
            levelBaru.add(kandang.get(i));
        }

        kandang = levelBaru;
    }
}